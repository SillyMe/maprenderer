﻿using Newtonsoft.Json;
using System.IO;

namespace MapRender.Core
{
    public static class StreamExtensions
    {
        public static T Deserialize<T>(this Stream source) {
            var serializer = new JsonSerializer();

            using (var sr = new StreamReader(source))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize<T>(jsonTextReader);
            }
        }

        public static void Serialize<T>(this Stream source, T value)
        {
            using (var writer = new StreamWriter(source))
            using (var jsonWriter = new JsonTextWriter(writer))
            {
                var ser = new JsonSerializer();
                ser.Serialize(jsonWriter, value);
            }
        }
    }
}
