﻿using System.Collections.Generic;

namespace MapRender.Core.Entities.Rendering
{
	public class RenderMap
	{
		public string ClientName { get; set; }

		public string Name { get; set; }

		public IEnumerable<RenderArea> Areas { get; set; }
	}
}
