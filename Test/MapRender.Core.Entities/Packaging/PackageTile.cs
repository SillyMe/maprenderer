﻿namespace MapRender.Core.Entities.Packaging
{
	public class PackageTileInfo
	{
		public string FilePath { get; set; }

		public int Y { get; set; }

		public int X { get; set; }

		public int ZoomLevel { get; set; }
	}
}
