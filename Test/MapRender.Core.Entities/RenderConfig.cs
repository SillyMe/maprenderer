﻿using System.Collections.Generic;
using MapRender.Core.Entities.Rendering;

namespace MapRender.Core.Entities
{
	public class RenderConfig
	{
		public IEnumerable<RenderMap> Maps { get; set; }
	}
}
