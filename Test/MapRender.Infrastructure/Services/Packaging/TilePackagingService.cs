﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GMap.NET;
using MapRender.Core.Entities.Packaging;

namespace MapRender.Infrastructure.Services.Packaging
{
	[Export(typeof(ITilePackagingService))]
	[PartCreationPolicy(CreationPolicy.Shared)]
	public class TilePackagingService : ITilePackagingService
	{
		public async Task<PackageResult> Pack(string folderPath, CancellationToken cancellationToken)
		{
			var result = new PackageResult();

			try
			{
				// TODO: Remove
#if DEBUG
				folderPath = @"D:\Temp\Tiles";
#endif

				// Execute as task to prevent UI freezes. TODO: Ensure that works.
				await Task.Factory.StartNew(() =>
				{
					var tiles = GetTiles(folderPath);
					PackTilesIntoDb(tiles);
				}, cancellationToken);

				result.Success = true;
			}
			catch (Exception)
			{
				result.Success = false;
			}

			return new PackageResult();
		}

		/// <summary>
		/// Scan Render output folder and gather information about tiles.
		/// </summary>
		private static IEnumerable<PackageTileInfo> GetTiles(string folderPath)
		{
			var directoryInfo = new DirectoryInfo(folderPath);

			var tiles = directoryInfo.EnumerateDirectories()
				.SelectMany(x =>
				{
					var zoomDirectoryInfo = new DirectoryInfo(x.FullName);

					return zoomDirectoryInfo.EnumerateDirectories()
						.SelectMany(y =>
						{
							var xDirectoryInfo = new DirectoryInfo(y.FullName);

							return xDirectoryInfo.EnumerateFiles()
								.Select(z => new PackageTileInfo
								{
									FilePath = z.FullName,
									Y = int.Parse(Path.GetFileNameWithoutExtension(z.FullName)),
									X = int.Parse(xDirectoryInfo.Name),
									ZoomLevel = int.Parse(zoomDirectoryInfo.Name)
								});
						});
				});

			return tiles;
		}

		private void PackTilesIntoDb(IEnumerable<PackageTileInfo> tiles)
		{
			// TODO: Clean Up and store in separate file, not globally

			// Clear all, take fresh tiles
			GMaps.Instance.PrimaryCache.DeleteOlderThan(DateTime.MaxValue, null);

			foreach (var tile in tiles)
			{
				var gpoint = new GPoint(tile.X, tile.Y);

				// TODO: I think it is not working. Try to give PureImage check output file with SQLite viewer
				var image = File.ReadAllBytes(tile.FilePath);

				GMaps.Instance.PrimaryCache.PutImageToCache(image, 0, gpoint, tile.ZoomLevel);
			}
		}
	}
}
