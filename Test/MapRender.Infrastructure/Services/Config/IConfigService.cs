﻿using MapRender.Core.Entities;

namespace MapRender.Infrastructure.Services.Config
{
	public interface IConfigService
	{
		MrConfig Config { get; }
	}
}
