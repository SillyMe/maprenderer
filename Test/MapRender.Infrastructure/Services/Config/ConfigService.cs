﻿using System.ComponentModel.Composition;
using System.IO;
using MapRender.Core.Entities;
using Newtonsoft.Json;

namespace MapRender.Infrastructure.Services.Config
{
	[Export(typeof(IConfigService))]
	[PartCreationPolicy(CreationPolicy.Shared)]
	public class ConfigService : IConfigService
	{
		private readonly string _configFileName = "config.json";

		public ConfigService()
		{
			Load();
		}

		public MrConfig Config { get; private set; }

		private void Load()
		{
			if (File.Exists(_configFileName))
			{
				var text = File.ReadAllText(_configFileName);
				Config = JsonConvert.DeserializeObject<MrConfig>(text);
			}
			else
			{
				Config = new MrConfig();
				Save();
			}
			Config.PropertyChanged += (sender, args) => Save();
		}

		private void Save()
		{
			var text = JsonConvert.SerializeObject(Config, Formatting.Indented);
			File.WriteAllText(_configFileName, text);
		}
	}
}
