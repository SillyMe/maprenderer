﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using MapRender.Core.Entities;
using MapRender.Core.Entities.Rendering;
using MapRender.Infrastructure.Services.Config;

namespace MapRender.Infrastructure.Services.Rendering
{
	[Export(typeof(ITileRenderingService))]
	[PartCreationPolicy(CreationPolicy.Shared)]
	public class TileRenderingService : ITileRenderingService
	{
		private readonly MrConfig _config;

		[ImportingConstructor]
		public TileRenderingService(IConfigService configService)
		{
			_config = configService.Config;
		}

		public async Task<RenderResult> Render(RenderMap map, CancellationToken cancellationToken)
		{
			var subFolder = PrepareOutputFolder(map, _config.OuputFolder);

			var renderResult = new RenderResult
			{
				OutputFolder = subFolder,
				Map = map
			};

			var stopWatch = new Stopwatch();
			stopWatch.Start();

			foreach (var area in map.Areas)
			{
				var process = CreateAndStartRenderProcess(_config.PythonExeFilePath, _config.PythonScriptFilePath, _config.PostgresConnectionString, _config.NumberOfThreads, area);
				if (process == null)
					return renderResult;

				try
				{
					// Check if process still running
					while (true)
					{
						if (process.HasExited)
							break;

						await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken);
					}

					renderResult.Success = true;
				}
				catch (TaskCanceledException)
				{
					process.Kill();
					renderResult.Success = false;
					break;
				}
				catch (Exception)
				{
					renderResult.Success = false;
					break;
				}
			}

			stopWatch.Stop();
			renderResult.Elapsed = stopWatch.Elapsed;

			return renderResult;
		}

		private static string PrepareOutputFolder(RenderMap map, string outputFolder)
		{
			var subFolder = Path.Combine(outputFolder, map.Name);
			Directory.CreateDirectory(subFolder);
			return subFolder;
		}

		private static Process CreateAndStartRenderProcess(string pythonExeFilePath, string scriptFilePath, string connectionString, int numberOfThreads, RenderArea area)
		{
			var processStartInfo = new ProcessStartInfo
			{
				FileName = pythonExeFilePath,
				CreateNoWindow = false,
				UseShellExecute = false,
				Arguments = $"{scriptFilePath} {area.Xstart} {area.Xend} {area.Ystart} {area.Yend} {area.MinZoom} {area.MaxZoom} \"{connectionString}\" {numberOfThreads}"
			};

			var process = Process.Start(processStartInfo);
			return process;
		}
	}
}
