﻿using System.Threading;
using System.Threading.Tasks;
using MapRender.Core.Entities.Rendering;

namespace MapRender.Infrastructure.Services.Rendering
{
	public interface ITileRenderingService
	{
		Task<RenderResult> Render(RenderMap map, CancellationToken cancellationToken);
	}
}
