﻿using System.IO;

namespace MapRender.App.Models.Rendering
{
	public class RenderProfile
	{
		public bool Selected { get; set; }

		public string FilePath { get; set; }

		public string Name => Path.GetFileNameWithoutExtension(FilePath);
	}
}
