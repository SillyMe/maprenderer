﻿using System.ComponentModel.Composition;

namespace MapRender.App.Screens.Home
{
    [Export(nameof(HomeView))]
	[PartCreationPolicy(CreationPolicy.Shared)]
    public partial class HomeView
    {
		[ImportingConstructor]
        public HomeView(HomeViewModel vm)
		{
			DataContext = vm;
            InitializeComponent();
        }
    }
}
