﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using MahApps.Metro.Controls.Dialogs;
using MapRender.App.Models.Rendering;
using MapRender.Core.Entities;
using MapRender.Core.Entities.Rendering;
using MapRender.Infrastructure.Services.Config;
using MapRender.Infrastructure.Services.Rendering;
using MvvmDialogs;
using MvvmDialogs.FrameworkDialogs.FolderBrowser;
using Newtonsoft.Json;
using Prism.Commands;
using System.Threading.Tasks;
using MapRender.Infrastructure.Services.Packaging;
using MvvmDialogs.FrameworkDialogs.OpenFile;

namespace MapRender.App.Screens.Home
{
	[Export]
	[PartCreationPolicy(CreationPolicy.NonShared)]
	public class HomeViewModel : BaseViewModel
	{
		private readonly IDialogCoordinator _dialogCoordinator;
		private readonly IDialogService _dialogService;
		private readonly ITileRenderingService _tileRenderingService;
		private readonly ITilePackagingService _tilePackagingService;

		private readonly MrConfig _config;

		private readonly FileSystemWatcher _watcher;

		private const string FileFilter = "*.config";

		[ImportingConstructor]
		public HomeViewModel(
			IDialogCoordinator dialogCoordinator,
			IDialogService dialogService,
			IConfigService configService,
			ITileRenderingService tileRenderingService,
			ITilePackagingService tilePackagingService)
		{
			_dialogCoordinator = dialogCoordinator;
			_dialogService = dialogService;
			_tileRenderingService = tileRenderingService;
			_tilePackagingService = tilePackagingService;
			_config = configService.Config;

			_watcher = new FileSystemWatcher
			{
				Filter = FileFilter,
				Path = _config.ConfigFolder
			};

			_watcher.Changed += WatcherOnChanged;
			_watcher.Created += WatcherOnChanged;
			_watcher.Deleted += WatcherOnChanged;
			_watcher.Renamed += WatcherOnChanged;

			_watcher.EnableRaisingEvents = true;

			RenderProfiles = new ObservableCollection<RenderProfile>();
			DisplayRenderProfiles(_watcher.Path);
		}

		public int NumberOfThreads
		{
			get => _config.NumberOfThreads;
			set { _config.NumberOfThreads = value; OnPropertyChanged(); }
		}

		public bool RenderMapsParallelly
		{
			get => _config.RenderMapsParallelly;
			set { _config.RenderMapsParallelly = value; OnPropertyChanged(); }
		}

		public string ConfigFolder
		{
			get => _config.ConfigFolder;
			set
			{
				_config.ConfigFolder = value;
				_watcher.Path = value;
				DisplayRenderProfiles(value);
				OnPropertyChanged();
			}
		}

		public string OutputFolder
		{
			get => _config.OuputFolder;
			set { _config.OuputFolder = value; OnPropertyChanged(); }
		}

		public string PythonExeFilePath
		{
			get => _config.PythonExeFilePath;
			set { _config.PythonExeFilePath = value; OnPropertyChanged(); }
		}

		public string PostgresConnectionString
		{
			get => _config.PostgresConnectionString;
			set { _config.PostgresConnectionString = value; OnPropertyChanged(); }
		}

		public string PythonScriptFilePath
		{
			get => _config.PythonScriptFilePath;
			set { _config.PythonScriptFilePath = value; OnPropertyChanged(); }
		}

		public ObservableCollection<RenderProfile> RenderProfiles
		{
			get => _renderProfiles;
			set { _renderProfiles = value; OnPropertyChanged(); }
		}

		private DelegateCommand _selectPythonExe;
		public DelegateCommand SelectPythonExe => _selectPythonExe ?? (_selectPythonExe = new DelegateCommand(SelectPythonExeExecutor));

		private DelegateCommand _selectPythonScript;
		public DelegateCommand SelectPythonScript => _selectPythonScript ?? (_selectPythonScript = new DelegateCommand(SelectPythonScriptExecutor));

		private DelegateCommand _selectConfigFolder;
		public DelegateCommand SelectConfigFolder => _selectConfigFolder ?? (_selectConfigFolder = new DelegateCommand(SelectConfigFolderExecutor));

		private DelegateCommand _selectOutputFolder;
		public DelegateCommand SelectOutputFolder => _selectOutputFolder ?? (_selectOutputFolder = new DelegateCommand(SelectOutputFolderExecutor));

		private DelegateCommand _startRenderingCommand;
		public DelegateCommand StartRenderingCommand => _startRenderingCommand ?? (_startRenderingCommand = new DelegateCommand(SelectRenderingExecutor));

		private ObservableCollection<RenderProfile> _renderProfiles;

		private void SelectPythonExeExecutor()
		{
			var settings = new OpenFileDialogSettings
			{
				FileName = "python.exe",
				Filter = "Python exe|python.exe",
				Title = "Select Python EXE",
				CheckFileExists = true
			};

			var res = _dialogService.ShowOpenFileDialog(this, settings);
			if (res == true)
				PythonExeFilePath = settings.FileName;
		}

		private void SelectPythonScriptExecutor()
		{
			var settings = new OpenFileDialogSettings
			{
				Filter = "Python script|*.py",
				Title = "Select Python Scripts",
				CheckFileExists = true
			};

			var res = _dialogService.ShowOpenFileDialog(this, settings);
			if (res == true)
				PythonScriptFilePath = settings.FileName;
		}

		private void SelectOutputFolderExecutor()
		{
			var settings = new FolderBrowserDialogSettings();
			var res = _dialogService.ShowFolderBrowserDialog(this, settings);
			if (res == true)
				OutputFolder = settings.SelectedPath;
		}

		public void SelectConfigFolderExecutor()
		{
			var settings = new FolderBrowserDialogSettings();
			var res = _dialogService.ShowFolderBrowserDialog(this, settings);
			if (res == true)
				ConfigFolder = settings.SelectedPath;
		}

		private async void SelectRenderingExecutor()
		{
			var profiles = RenderProfiles.Where(x => x.Selected).ToList();

			if (profiles.Count == 0)
			{
				await _dialogCoordinator.ShowMessageAsync(this, "Error", "Please select Profiles to Render");
				return;
			}

			var maps = profiles.Select(x => JsonConvert.DeserializeObject<RenderMap>(File.ReadAllText(x.FilePath)));

			var ctSource = new CancellationTokenSource();
			var controller = await _dialogCoordinator.ShowProgressAsync(this, "Working", "Please wait...", true);
			controller.Canceled += (sender, args) => ctSource.Cancel();

			controller.SetIndeterminate();

			IEnumerable<OperationResult> operationResults = new List<OperationResult>();
			try
			{
				if (RenderMapsParallelly)
					operationResults = await RunInParallel(maps, ctSource.Token);
				else
					operationResults = await RunInSequence(maps, ctSource.Token);
			}
			catch (TaskCanceledException)
			{
				// Rendering was canceled
			}
			finally
			{
				await controller.CloseAsync();
				await DisplayResult(operationResults);
			}
		}

		private async Task<IEnumerable<OperationResult>> RunInSequence(IEnumerable<RenderMap> maps, CancellationToken cancellationToken)
		{
			var results = new List<OperationResult>();

			foreach (var map in maps)
			{
				var renderResult = await _tileRenderingService.Render(map, cancellationToken);
				var packageResult = await _tilePackagingService.Pack(renderResult.OutputFolder, cancellationToken);

				results.Add(new OperationResult
				{
					RenderResult = renderResult,
					PackageResult = packageResult
				});
			}

			return results;
		}

		private async Task<IEnumerable<OperationResult>> RunInParallel(IEnumerable<RenderMap> maps, CancellationToken cancellationToken)
		{
			var tasks = maps.AsParallel().Select(async x =>
			{
				var renderResult = await _tileRenderingService.Render(x, cancellationToken);
				var packageResult = await _tilePackagingService.Pack(renderResult.OutputFolder, cancellationToken);

				return new OperationResult
				{
					RenderResult = renderResult,
					PackageResult = packageResult
				};
			});

			return await Task.WhenAll(tasks);
		}

		private async Task DisplayResult(IEnumerable<OperationResult> renderResults)
		{
			var message = string.Join(Environment.NewLine + Environment.NewLine,
				renderResults.Select(x => $"{x.RenderResult.Map.Name}: {x.RenderResult.Success}, {x.RenderResult.Elapsed}"));

			if (string.IsNullOrEmpty(message))
				return;

			await _dialogCoordinator.ShowMessageAsync(this, "Result", message);
		}

		private void DisplayRenderProfiles(string path)
		{
			var directoryInfo = new DirectoryInfo(path);
			var files = directoryInfo.EnumerateFiles(FileFilter);

			var removedFiles = RenderProfiles.Where(x => files.All(y => y.Name != x.Name)).Select(x => RenderProfiles.IndexOf(x));
			var addedFiles = files.Where(x => RenderProfiles.All(y => y.Name != x.Name));

			Application.Current.Dispatcher.Invoke(() =>
			{
				removedFiles
					.Reverse()
					.ToList()
					.ForEach(RenderProfiles.RemoveAt);

				RenderProfiles.AddRange(addedFiles.Select(x => new RenderProfile
				{
					FilePath = x.FullName
				}));
			});
		}

		private void WatcherOnChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
		{
			var watcher = (FileSystemWatcher)sender;
			DisplayRenderProfiles(watcher.Path);
		}
	}
}
