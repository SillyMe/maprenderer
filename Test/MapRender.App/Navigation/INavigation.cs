﻿namespace MapRender.App.Navigation
{
    public interface INavigation
    {
        void Navigate(ShellScreen screen);
    }
}
