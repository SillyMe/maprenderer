﻿using System.Windows;

namespace MapRender.App
{
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			new Bootstrapper().Run(true);
			base.OnStartup(e);
		}
	}
}
