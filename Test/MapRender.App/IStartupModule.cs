﻿namespace MapRender.App
{
    public interface IStartupModule
    {
	    void Start();
    }
}
