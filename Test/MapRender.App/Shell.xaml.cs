﻿using System.ComponentModel.Composition;

namespace MapRender.App
{
	[Export]
	[PartCreationPolicy(CreationPolicy.Shared)]
	public partial class Shell
	{
		public Shell()
		{
			InitializeComponent();
		}
	}
}
