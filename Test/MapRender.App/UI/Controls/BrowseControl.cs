﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Prism.Commands;

namespace MapRender.App.UI.Controls
{
	public class BrowseControl : UserControl
	{
		public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
			"Text", typeof (string), typeof (BrowseControl), new PropertyMetadata(default(string)));

		public string Text
		{
			get { return (string) GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public static readonly DependencyProperty ButtonCommandProperty = DependencyProperty.Register(
			"ButtonCommand", typeof (DelegateCommand), typeof (BrowseControl), new PropertyMetadata(default(DelegateCommand)));

		public ICommand ButtonCommand
		{
			get { return (DelegateCommand) GetValue(ButtonCommandProperty); }
			set { SetValue(ButtonCommandProperty, value); }
		}

		public static readonly DependencyProperty ReadOnlyProperty = DependencyProperty.Register(
			"ReadOnly", typeof (bool), typeof (BrowseControl), new PropertyMetadata(default(bool)));

		public bool ReadOnly
		{
			get { return (bool) GetValue(ReadOnlyProperty); }
			set { SetValue(ReadOnlyProperty, value); }
		}
	}
}
