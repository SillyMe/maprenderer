﻿using System.ComponentModel.Composition;
using MapRender.App.Navigation;

namespace MapRender.App
{
	[Export(typeof(IStartupModule))]
    public class Startup : IStartupModule
    {
	    private readonly INavigation _navigation;

	    [ImportingConstructor]
	    public Startup(INavigation navigation)
	    {
		    _navigation = navigation;
	    }

	    public void Start()
	    {
			_navigation.Navigate(ShellScreen.Home);
	    }
    }
}
