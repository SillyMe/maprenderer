﻿using System;
using System.ComponentModel.Composition;
using MapRender.App.Screens.Home;
using Prism.Regions;

namespace MapRender.App.Navigation
{
    [Export(typeof(INavigation))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    internal class Navigation : INavigation
    {
        private readonly IRegionManager _regionManager;

        [ImportingConstructor]
        internal Navigation(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void Navigate(ShellScreen screen)
        {
            switch (screen)
            {
				case ShellScreen.Home:
					_regionManager.RequestNavigate(ShellRegion.SingleRegion, nameof(HomeView));
					break;
				default:
                    throw new ArgumentOutOfRangeException($"{screen} is not configured in {nameof(Navigation)}");
            }
        }
    }
}
