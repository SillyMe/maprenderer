﻿using MahApps.Metro.Controls.Dialogs;
using MapRender.App.Models.Rendering;
using MapRender.Core;
using MapRender.Core.Entities;
using MapRender.Core.Entities.Rendering;
using MapRender.Infrastructure.Services.Config;
using MapRender.Infrastructure.Services.Packaging;
using MapRender.Infrastructure.Services.Rendering;
using MvvmDialogs;
using MvvmDialogs.FrameworkDialogs.FolderBrowser;
using MvvmDialogs.FrameworkDialogs.OpenFile;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MapRender.App.Screens.Home
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class HomeViewModel : BaseViewModel
    {
        private readonly IDialogCoordinator _dialogCoordinator;
        private readonly IDialogService _dialogService;
        private readonly ITileRenderingService _tileRenderingService;
        private readonly ITilePackagingService _tilePackagingService;

        private readonly MrConfig _config;

        private readonly FileSystemWatcher _watcher;

        private const string FileFilter = "*.config";

        [ImportingConstructor]
        public HomeViewModel(
            IDialogCoordinator dialogCoordinator,
            IDialogService dialogService,
            IConfigService configService,
            ITileRenderingService tileRenderingService,
            ITilePackagingService tilePackagingService)
        {
            _dialogCoordinator = dialogCoordinator;
            _dialogService = dialogService; 
            _tileRenderingService = tileRenderingService;
            _tilePackagingService = tilePackagingService;
            _config = configService.Config;

            _watcher = new FileSystemWatcher
            {
                Filter = FileFilter,
                Path = _config.ConfigFolder
            };

            _watcher.Changed += WatcherOnChanged;
            _watcher.Created += WatcherOnChanged;
            _watcher.Deleted += WatcherOnChanged;
            _watcher.Renamed += WatcherOnChanged;

           if(Directory.Exists(_watcher.Path)) _watcher.EnableRaisingEvents = true;

            RenderProfiles = new ObservableCollection<RenderProfile>();
            DisplayRenderProfiles(_watcher.Path);
            UpdateRednerAndPackResults();
        }

        public int NumberOfThreads
        {
            get => _config.NumberOfThreads;
            set { _config.NumberOfThreads = value; OnPropertyChanged(); }
        }

        public bool RenderMapsParallelly
        {
            get => _config.RenderMapsParallelly;
            set { _config.RenderMapsParallelly = value; OnPropertyChanged(); }
        }

        public string ConfigFolder
        {
            get => _config.ConfigFolder ?? "C:\\";
            set
            {
                value = string.IsNullOrEmpty(value) ? "C:\\" : value;
                _config.ConfigFolder = value;
                if(Directory.Exists(value)) _watcher.Path = value;
                DisplayRenderProfiles(value);
                UpdateRednerAndPackResults();
                OnPropertyChanged();
            }
        }

        private void UpdateRednerAndPackResults()
        {
            DisplayRenderProfiles(_watcher.Path);

            foreach (var profile in RenderProfiles)
            {
                var tilePath = profile.TilePath(_config.OuputFolder);
                if (string.IsNullOrEmpty(tilePath) || !Directory.Exists(tilePath))
                {
                    profile.Reset();
                    continue;
                }
                var gmdbFileName = Directory.GetFiles(tilePath).FirstOrDefault(f => f.EndsWith(".gmdb", StringComparison.OrdinalIgnoreCase));
                var gmdbFile = string.IsNullOrEmpty(gmdbFileName) ? null : new FileInfo(gmdbFileName);

                if(gmdbFile == null)
                {
                    profile.ResetGmdb();
                }
                else
                {
                    profile.GMDBExist = true;
                    profile.GmdbDate = gmdbFile.LastWriteTime;
                    profile.GmdbSize = HumanReadableSize(gmdbFile.Length);
                }
              
                var tileDirectoties = Directory.GetDirectories(tilePath);
                profile.TilesExist = tileDirectoties.Count() >= 12;
                profile.TileCount = 0;
                OnPropertyChanged();

                //foreach (var firstlvl in tileDirectoties)
                //{
                //    if (string.IsNullOrEmpty(firstlvl) || !Directory.Exists(firstlvl)) continue;
                //    foreach (var secondLevel in Directory.GetDirectories(firstlvl))
                //    {
                //        if (string.IsNullOrEmpty(secondLevel) || !Directory.Exists(secondLevel)) continue;
                //        profile.TileCount += Directory.GetFiles(secondLevel).Count(f => f.EndsWith(".png"));
                //    }
                //}
            }
        }

        private string HumanReadableSize(long bytes)
        {
            if (bytes <= 1024) return $"{bytes} B";
            if (bytes <= 1024 * 1024) return $"{bytes / 1024} KB";
            if (bytes <= 1024 * 1024 * 1024) return $"{bytes / 1024 / 1024} MB";
            return $"{bytes / 1024 / 1024 / 1024} GB";
        }

        public string OutputFolder
        {
            get => _config.OuputFolder;
            set { _config.OuputFolder = value; OnPropertyChanged(); }
        }

        public string PythonExeFilePath
        {
            get => _config.PythonExeFilePath;
            set { _config.PythonExeFilePath = value; OnPropertyChanged(); }
        }

        public string PythonScriptFilePath
        {
            get => _config.PythonScriptFilePath;
            set { _config.PythonScriptFilePath = value; OnPropertyChanged(); }
        }

        public ObservableCollection<RenderProfile> RenderProfiles
        {
            get => _renderProfiles;
            set { _renderProfiles = value; OnPropertyChanged(); }
        }

        private DelegateCommand _selectPythonScript;
        public DelegateCommand SelectPythonScript => _selectPythonScript ?? (_selectPythonScript = new DelegateCommand(SelectPythonScriptExecutor));

        private DelegateCommand _selectPythonExe;
        public DelegateCommand SelectPythonExe => _selectPythonExe ?? (_selectPythonExe = new DelegateCommand(_selectPythonExeExecutor));

        private DelegateCommand _selectConfigFolder;
        public DelegateCommand SelectConfigFolder => _selectConfigFolder ?? (_selectConfigFolder = new DelegateCommand(SelectConfigFolderExecutor));

        private DelegateCommand _selectOutputFolder;
        public DelegateCommand SelectOutputFolder => _selectOutputFolder ?? (_selectOutputFolder = new DelegateCommand(SelectOutputFolderExecutor));

        private DelegateCommand _startRenderingCommand;
        public DelegateCommand StartRenderingCommand => _startRenderingCommand ?? (_startRenderingCommand = new DelegateCommand(() => SelectRenderingExecutor(true, true)));

        private DelegateCommand _sartOnlyRenderingCommand;
        public DelegateCommand StartOnlyRenderingCommand => _sartOnlyRenderingCommand ?? (_sartOnlyRenderingCommand = new DelegateCommand(() => SelectRenderingExecutor(true, false)));

        private DelegateCommand _startOnlyPacking;
        public DelegateCommand StartOnlyPacking => _startOnlyPacking ?? (_startOnlyPacking = new DelegateCommand(() => SelectRenderingExecutor(false, true)));

        private ObservableCollection<RenderProfile> _renderProfiles;

        private void SelectPythonScriptExecutor()
        {
            var settings = new OpenFileDialogSettings
            {
                Filter = "Python script|*.py",
                Title = "Select Python Scripts",
                CheckFileExists = true
            };

            var res = _dialogService.ShowOpenFileDialog(this, settings);
            if (res == true)
                PythonScriptFilePath = settings.FileName;
        }

        private void _selectPythonExeExecutor()
        {
            var settings = new OpenFileDialogSettings
            {
                Filter = "Python script|*.py",
                Title = "Select Python Scripts",
                CheckFileExists = true
            };

            var res = _dialogService.ShowOpenFileDialog(this, settings);
            if (res == true)
                PythonExeFilePath = settings.FileName;
        }

        private void SelectOutputFolderExecutor()
        {
            var settings = new FolderBrowserDialogSettings();
            var res = _dialogService.ShowFolderBrowserDialog(this, settings);
            if (res == true)
                OutputFolder = settings.SelectedPath;
        }

        public void SelectConfigFolderExecutor()
        {
            var settings = new FolderBrowserDialogSettings();
            var res = _dialogService.ShowFolderBrowserDialog(this, settings);
            if (res == true)
                ConfigFolder = settings.SelectedPath;
        }

        /// <summary>
        /// Collects all maps that need to be rendered and/or packed in GMDB and actually does that
        /// </summary>
        /// <param name="render"></param>
        /// <param name="pack"></param>
        private async void SelectRenderingExecutor(bool render = true, bool pack = true)
        {
            var profiles = RenderProfiles.Where(x => x.Selected).ToList();

            if (profiles.Count == 0)
            {
                await _dialogCoordinator.ShowMessageAsync(this, "Error", "Please select Profiles to Render/Pack");
                return;
            }

            var maps = profiles.Select(x =>
            {
                using (var stream = new FileStream(x.FilePath, FileMode.Open, FileAccess.Read))
                {
                    return stream.Deserialize<RenderMap>();
                }
            });

            var ctSource = new CancellationTokenSource();
            var controller = await _dialogCoordinator.ShowProgressAsync(this, "Working", "Please wait...", true);
            controller.Canceled += (sender, args) => ctSource.Cancel();

            controller.SetIndeterminate();

            IEnumerable<OperationResult> operationResults = new List<OperationResult>();
            try
            {
                if (RenderMapsParallelly)
                    operationResults = await RenderAndPackInParallel(maps, ctSource.Token, render, pack);
                else
                    operationResults = await RenderAndPackInSequence(maps, ctSource.Token, render, pack);
            }
            catch (TaskCanceledException)
            {
                // Rendering was canceled
            }
            finally
            {
                await controller.CloseAsync();
                await DisplayResult(operationResults);
            }
        }

        private async Task<IEnumerable<OperationResult>> RenderAndPackInSequence(IEnumerable<RenderMap> maps, CancellationToken cancellationToken, bool render, bool pack)
        {
            var results = new List<OperationResult>();

            foreach (var map in maps)
            {
                var outputPath = RenderProfiles.FirstOrDefault(p => p.Name == map.Name).TilePath(_config.OuputFolder);
                var renderResult = await _tileRenderingService.Render(map, outputPath, cancellationToken);
                var packageResult = await _tilePackagingService.Pack(renderResult.OutputFolder, cancellationToken, _dialogCoordinator);

                results.Add(new OperationResult
                {
                    RenderResult = renderResult,
                    PackageResult = packageResult
                });
            }

            return results;
        }

        private async Task<IEnumerable<OperationResult>> RenderAndPackInParallel(IEnumerable<RenderMap> maps, CancellationToken cancellationToken, bool render, bool pack)
        {
            var tasks = maps.AsParallel().Select(async x =>
            {
                var outputPath = RenderProfiles.FirstOrDefault(p => p.Name == x.Name)?.TilePath(_config.OuputFolder) ?? TileRenderingService.PrepareOutputFolder(x, _config.OuputFolder);
                var renderResult = render ? await _tileRenderingService.Render(x, outputPath, cancellationToken) : new RenderResult()
                {
                    Map = x,
                    Success = true,
                    OutputFolder = outputPath //TileRenderingService.PrepareOutputFolder(x, _config.OuputFolder)
                };
                var packageResult = pack ? await _tilePackagingService.Pack(renderResult.OutputFolder, cancellationToken, _dialogCoordinator) : new Core.Entities.Packaging.PackageResult() { Success = true };

                return new OperationResult
                {
                    RenderResult = renderResult,
                    PackageResult = packageResult
                };
            });

            return await Task.WhenAll(tasks).ContinueWith((res) => { UpdateRednerAndPackResults(); return res.Result; });
        }

     

        private async Task DisplayResult(IEnumerable<OperationResult> renderResults)
        {
            var message = string.Join(Environment.NewLine + Environment.NewLine,
                renderResults.Select(x => $"{x.RenderResult.Map.Name}: {x.RenderResult.Success}, {x.RenderResult?.Elapsed ?? TimeSpan.Zero}"));

            if (string.IsNullOrEmpty(message))
                return;

            await _dialogCoordinator.ShowMessageAsync(this, "Result", message);
        }

        private void DisplayRenderProfiles(string path)
        {
            if (!Directory.Exists(path)) return;
            var directoryInfo = new DirectoryInfo(path);
            var files = directoryInfo.EnumerateFiles(FileFilter);

            var removedFiles = RenderProfiles.Where(x => files.All(y => y.Name != x.Name)).Select(x => RenderProfiles.IndexOf(x));
            var addedFiles = files.Where(x => RenderProfiles.All(y => y.Name != x.Name));

            Application.Current.Dispatcher.Invoke(() =>
            {
                removedFiles
                    .Reverse()
                    .ToList()
                    .ForEach(RenderProfiles.RemoveAt);

                RenderProfiles.AddRange(addedFiles.Select(x => new RenderProfile
                {
                    FilePath = x.FullName
                }));
            });
        }

        private void WatcherOnChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            var watcher = (FileSystemWatcher)sender;
            DisplayRenderProfiles(watcher.Path);
        }
    }
}
