﻿using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Windows.Controls;

namespace MapRender.App.Screens.Home
{
    [Export(nameof(HomeView))]
	[PartCreationPolicy(CreationPolicy.Shared)]
    public partial class HomeView
    {
		[ImportingConstructor]
        public HomeView(HomeViewModel vm)
		{
			DataContext = vm;
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var button = sender as Button;
            string filepath = button?.Tag?.ToString();
            if(Directory.Exists(filepath)) Process.Start(filepath);
        }
    }
}
