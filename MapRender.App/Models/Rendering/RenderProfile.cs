﻿using System;
using System.IO;

namespace MapRender.App.Models.Rendering
{
	public class RenderProfile
	{
		public bool Selected { get; set; }

		public string FilePath { get; set; }

		public string Name => Path.GetFileNameWithoutExtension(FilePath);

        public bool TilesExist { get; set; }
        public int TileCount { get; set; }
        public bool GMDBExist { get; set; }
        public string GmdbSize { get; set; }
        public DateTime GmdbDate { get; set; }
        public string GmdbDateString => GmdbDate.ToShortDateString();
        public string DefaultTilePath => TilePath();

        public string TilePath(string baseFolder = "")
        {
            if (string.IsNullOrEmpty(baseFolder)) baseFolder = Path.Combine(Directory.GetParent(FilePath).Parent.FullName, "Tiles");
            if (string.IsNullOrEmpty(Name) || !Directory.Exists(baseFolder)) return string.Empty;
            return Path.Combine(baseFolder, Name.Trim().Replace(" ", "_").Replace("ö", "oe").Replace("ü", "ue").Replace("ä", "ae"));
        }

        public void Reset()
        {
            TilesExist = false;
            TileCount = 0;
            ResetGmdb();
        }

        public void ResetGmdb()
        {
            GMDBExist = false;
            GmdbSize = string.Empty;
            GmdbDate = DateTime.MinValue;
        }
    }
}
