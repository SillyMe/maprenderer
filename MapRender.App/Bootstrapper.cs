﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Registration;
using System.Reflection;
using System.Windows;
using log4net;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Practices.ServiceLocation;
using MvvmDialogs;
using MvvmDialogs.DialogFactories;
using MvvmDialogs.DialogTypeLocators;
using Prism.Mef;
using MapRender.Infrastructure.Services.Config;
using MvvmDialogs.FrameworkDialogs;

namespace MapRender.App
{
    public class Bootstrapper : MefBootstrapper
    {
	    private readonly ILog _logger = LogManager.GetLogger(typeof(Bootstrapper));

	    protected override DependencyObject CreateShell()
	    {
		    return Container.GetExportedValue<Shell>();
	    }

	    protected override void InitializeShell()
	    {
		    base.InitializeShell();
		    Application.Current.MainWindow = (Shell)Shell;
		    Application.Current.MainWindow.Show();
	    }

	    protected override void ConfigureAggregateCatalog()
	    {
		    base.ConfigureAggregateCatalog();
		    var executingAssembly = Assembly.GetExecutingAssembly();
		    // Use current assembly when looking for MEF exports
		    AggregateCatalog.Catalogs.Add(new AssemblyCatalog(executingAssembly));

            // Register Dialogs from MahApps
		    var registration = new RegistrationBuilder();
		    registration.ForType<DialogCoordinator>().Export<IDialogCoordinator>();
		    var catalog = new AssemblyCatalog(typeof(DialogCoordinator).Assembly, registration);
		    AggregateCatalog.Catalogs.Add(catalog);

            // Register Open and Save Dialogs from MVVMDialogs
			registration = new RegistrationBuilder();
            registration.ForType<DialogService>().Export<IDialogService>();
            registration.ForType<DefaultFrameworkDialogFactory>().Export<IFrameworkDialogFactory>();
            registration.ForType<NamingConventionDialogTypeLocator>().Export<IDialogTypeLocator>();
            registration.ForType<ReflectionDialogFactory>().Export<IDialogFactory>();
            catalog = new AssemblyCatalog(typeof(DialogService).Assembly, registration);
			AggregateCatalog.Catalogs.Add(catalog);

			AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(ConfigService).Assembly));
		}

	    protected override CompositionContainer CreateContainer()
	    {
		    var container = base.CreateContainer();
		    container.ComposeExportedValue(container);
		    return container;
	    }

	    public override void Run(bool runWithDefaultConfiguration)
	    {
		    try
		    {
			    base.Run(runWithDefaultConfiguration);

                var startupModule = ServiceLocator.Current.GetInstance<IStartupModule>();
			    startupModule?.Start();
		    }

		    catch (ReflectionTypeLoadException ex)
		    {
			    foreach (var e in ex.LoaderExceptions)
			    {
					_logger.Error(e);
				}
		    }
		    catch (CompositionException ex)
		    {
			    foreach (var e in ex.RootCauses)
			    {
					_logger.Error(e);
				}
		    }
	    }
	}
}
