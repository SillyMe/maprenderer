﻿using System.Threading;
using System.Threading.Tasks;
using MapRender.Core.Entities.Packaging;
using MahApps.Metro.Controls.Dialogs;
namespace MapRender.Infrastructure.Services.Packaging
{
	public interface ITilePackagingService
	{
		Task<PackageResult> Pack(string folderPath, CancellationToken cancellationToken, IDialogCoordinator dialogCoordinator);
    }
}
