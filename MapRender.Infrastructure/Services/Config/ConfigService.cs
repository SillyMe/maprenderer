﻿using System.ComponentModel.Composition;
using System.IO;
using MapRender.Core.Entities;
using MapRender.Core;

namespace MapRender.Infrastructure.Services.Config
{
    [Export(typeof(IConfigService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ConfigService : IConfigService
    {
        private readonly string _configFileName = "config.json";

        public ConfigService()
        {
            Load();
        }

        public MrConfig Config { get; private set; }

        private void Load()
        {
            if (File.Exists(_configFileName))
            {
                using (var stream = new FileStream(_configFileName, FileMode.Open, FileAccess.Read))
                {
                    Config = stream.Deserialize<MrConfig>();
                }
            }
            else
            {
                Config = new MrConfig();
                Save();
            }
            Config.PropertyChanged += (sender, args) => Save();
        }

        private void Save()
        {
            using (var stream = new FileStream(_configFileName, FileMode.Create, FileAccess.Write))
            {
                stream.Serialize(Config);
            }
        }
    }
}
