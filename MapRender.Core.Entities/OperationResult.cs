﻿using MapRender.Core.Entities.Packaging;
using MapRender.Core.Entities.Rendering;

namespace MapRender.Core.Entities
{
	public class OperationResult
	{
		public RenderResult RenderResult { get; set; }

		public PackageResult PackageResult { get; set; }
	}
}
