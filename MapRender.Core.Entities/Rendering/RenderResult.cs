﻿using System;

namespace MapRender.Core.Entities.Rendering
{
	public class RenderResult
	{
		public RenderMap Map { get; set; }

		public int TileCount { get; set; }

		public TimeSpan Elapsed { get; set; }

		public string OutputFolder { get; set; }

		public bool Success { get; set; }
	}
}
