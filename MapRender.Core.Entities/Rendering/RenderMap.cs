﻿using System.Collections.Generic;

namespace MapRender.Core.Entities.Rendering
{
	public class RenderMap
	{
		public string ClientName { get; set; }

        public string _name;
        public string Name { get { return _name; } set { _name = value; } }

		public IEnumerable<RenderArea> Areas { get; set; }
	}
}
