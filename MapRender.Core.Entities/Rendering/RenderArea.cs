﻿namespace MapRender.Core.Entities.Rendering
{
	public class RenderArea
	{
		public decimal Xstart { get; set; }

		public decimal Xend { get; set; }

		public decimal Ystart { get; set; }

		public decimal Yend { get; set; }

		public int MinZoom { get; set; }

		public int MaxZoom{ get; set; }
	}
}
