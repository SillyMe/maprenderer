﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MapRender.Core.Entities
{
	public class MrConfig : INotifyPropertyChanged
	{
		private int _numberOfThreads;
	    private string _configFolder;
		private string _ouputFolder;
		private bool _renderMapsParallelly;
		private string _pythonScriptFilePath;
        private string _pythonExeFilePath;
        
        public int NumberOfThreads
		{
			get => _numberOfThreads;
			set { _numberOfThreads = value; OnPropertyChanged(); }
		}

		public string ConfigFolder
		{
			get => _configFolder ?? "C:\\";
			set { _configFolder = value; OnPropertyChanged(); }
		}

		public string OuputFolder
		{
			get => _ouputFolder.Replace(" ", "_");
			set { _ouputFolder = value; OnPropertyChanged(); }
		}

		public bool RenderMapsParallelly
		{
			get => _renderMapsParallelly;
			set { _renderMapsParallelly = value; OnPropertyChanged(); }
		}

        public string PythonExeFilePath
        {
            get => _pythonExeFilePath;
            set { _pythonExeFilePath = value; OnPropertyChanged(); }
        }
        
        public string PythonScriptFilePath
		{
			get => _pythonScriptFilePath;
			set { _pythonScriptFilePath = value; OnPropertyChanged(); }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
