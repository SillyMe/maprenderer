﻿using System.IO;

namespace MapRender.Core.Entities.Packaging
{
	public class PackageResult
	{
		public string Folder { get; set; }

		public string FilePath { get; set; }

		public string FileName => Path.GetFileNameWithoutExtension(FilePath);

		public bool Success { get; set; }
	}
}
